<!DOCTYPE html>
<html>

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <!--og-->
  <meta property="og:title" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">
  <meta property="og:type" content="website">
  <meta property="og:description" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">
  <meta property="og:url" content="https://example.com/">
  <meta property="og:site_name" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">

  <meta charset="UTF-8">
  <title>スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー</title>
  <meta name="keywords" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">
  <meta name="description" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

  <link rel="canonical" href="https://example.com/">
  <link rel="stylesheet" href="css/main.css" />
  <link rel="apple-touch-icon" href="img/common/apple.png">

  <!--image max5-->
  <meta property="og:image" content="https://example.com/img/common/OG.png">

  <!--Twitter-->
  <meta name="twitter:card" content="summary_large_image">

  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css">
  <script src="js/common.js"></script>
</head>

<body class="top">
  <header id="global-head">
    <div class="header-text">
      <div class="container">
        <p>あと〇箇所でスペシャルルーレットにチャレンジ可能</p>
      </div>
    </div>
    <h1><a href="index.php"><img src="img/common/main-logo01.png" alt="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー"></a></h1>
    <div class="container">
      <div class="keihin">
        <h2><img src="img/common/header-text01.png" alt="景品一覧"></h2>
        <div class="list">
          <dl>
            <dt class="num01">1等</dt>
            <dd>10000pts</dd>
            <dt class="num02">2等</dt>
            <dd>3000pts</dd>
            <dt class="num03">3等</dt>
            <dd>1000pts</dd>
            <dt class="num04">4等</dt>
            <dd>200pts</dd>
          </dl>
        </div>
      </div>
      <div class="btn"><a href="challenge_point.php" class="btn01 green">チャレンジポイントはこちら</a></div>
    </div>
  </header>

  <main>
    <div class="container">
      <div class="top-content01">
        <ul class="menu">
          <li><a href="#modal-rule" class="noScroll" rel="modal:open"><img src="img/top/menu01.png" alt="ルール"></a></li>
          <li><a href="#modal-notes" class="noScroll" rel="modal:open"><img src="img/top/menu02.png" alt="注意事項"></a></li>
          <li><a href="#modal-nfc" class="noScroll" rel="modal:open"><img src="img/top/menu03.png" alt="NFC"></a></li>
        </ul>
        <div class="btn"><a href="#modal-peach-point" class="btn01 noScroll" rel="modal:open">ピーチポイントに引き換える</a></div>
      </div>

      <section class="top-content02">
        <h2 class="title01"><span>最近の獲得履歴</span></h2>
        <ul class="point-list">
          <li>
            <figure><img src="img/top/point04_top.svg" alt="4等 200pts"></figure>
            <div class="point">
              <h3>釧路フィッシャーマンズワーフMOO</h3>
              <p class="date">2019-06-28 14:35</p>
              <p class="get">200</p>
            </div>
          </li>
          <li>
            <figure><img src="img/top/point04_top.svg" alt="4等 200pts"></figure>
            <div class="point">
              <h3>釧路観光クルーズ船　SEA CRANE</h3>
              <p class="date">2019-06-28 14:35</p>
              <p class="get">200</p>
            </div>
          </li>
          <li>
            <figure><img src="img/top/point03_top.svg" alt="3等 1,000pts"></figure>
            <div class="point">
              <h3>釧路市動物園</h3>
              <p class="date">2019-06-28 14:35</p>
              <p class="get">1000</p>
            </div>
          </li>
          <li>
            <figure><img src="img/top/point02_top.svg" alt="2等 3,000pts"></figure>
            <div class="point">
              <h3>釧路フィッシャーマンズワーフMOO</h3>
              <p class="date">2019-06-28 14:35</p>
              <p class="get">3000</p>
            </div>
          </li>
          <li>
            <figure><img src="img/top/point01_top.svg" alt="1等 10,000pts"></figure>
            <div class="point">
              <h3>釧路フィッシャーマンズワーフMOO</h3>
              <p class="date">2019-06-28 14:35</p>
              <p class="get">10000</p>
            </div>
          </li>
          <li>
            <figure><img src="img/top/point03_top.svg" alt="3等 1,000pts"></figure>
            <div class="point">
              <h3>釧路観光クルーズ船　SEA CRANE</h3>
              <p class="date">2019-06-28 14:35</p>
              <p class="get">1000</p>
            </div>
          </li>
          <li>
            <figure><img src="img/top/point04_top.svg" alt="4等 200pts"></figure>
            <div class="point">
              <h3>釧路市動物園</h3>
              <p class="date">2019-06-28 14:35</p>
              <p class="get">200</p>
            </div>
          </li>
          <li>
            <figure><img src="img/top/point04_top.svg" alt="4等 200pts"></figure>
            <div class="point">
              <h3>釧路フィッシャーマンズワーフMOO</h3>
              <p class="date">2019-06-28 14:35</p>
              <p class="get">200</p>
            </div>
          </li>
        </ul>
      </section>
    </div>

    <?php /* モーダル確認用 */?>
    <div class="text-center" style="margin-top: 150px;">
      <div class="container">
        <h2 class="title01"><span>モーダル表示確認</span></h2>
        <p><a href="#modal-already-touch" class="noScroll btn01" rel="modal:open">タッチ済み</a></p>
        <p><a href="#modal-form" class="noScroll btn01" rel="modal:open">入力フォーム</a></p>
        <p><a href="#modal-form-confirm" class="noScroll btn01" rel="modal:open">入力確認</a></p>
        <p><a href="#modal-form-thanks" class="noScroll btn01" rel="modal:open">ありがとうございました</a></p>
      </div>
    </div>
  </main>

  <footer id="global-foot">
    <nav>
      <ul class="container">
        <li><a href="index.php">TOP</a></li>
        <li><a href="challenge_point.php">チャレンジポイント</a></li>
      </ul>
    </nav>
    <div class="container">
      <p class="privacy"><a href="#">プライバシーポリシー</a></p>
      <small class="copylight">Copyright(C) 2019 Peach Aviation 株式会社</small>
    </div>
  </footer>

  <div id="page-top"><a href="#global-head"><img src="img/common/page-top.svg" alt="Page Top"></a></div>


<?php // ----------
// モーダルウィンドウ
// ---------------- ?>
<?php // modal#modal-rule?>
<div id="modal-rule" class="modal">
  <h2 class="title01"><span>ルール説明</span></h2>
  <p>
    ひがし北海道の50ヶ所のチェックポイントにあるスマートプレート（QRコード）」にスマホでタッチ
  </p>
  <p>
    ▼
  </p>
  <p>
    ルーレットチャレンジがスタート
  </p>
  <p>
    ▼
  </p>
  <p>
    1～5等までのポイントが当たる（はずれの場合もあり）
  </p>
  <p>
    ▼
  </p>
  <p>
    引換所の引き換え用スマプレを読み込む
  </p>
  <p>
    ▼
  </p>
  <p>
    ピーチポイントに引き換えのお申込みフォームが開く
  </p>
  <p>
    ▼
  </p>
  <p>
    お名前、会員情報を入力する
  </p>
  <p>
    ▼
  </p>
  <p>
    ピーチポイントが付与される
  </p>
  <div class="close-btn01"><p><a href="#close-modal" rel="modal:close" class="btn01 green">とじる</a></p></div>
</div>

<?php // modal#modal-notes?>
<div id="modal-notes" class="modal">
  <h2 class="title01"><span>注意事項</span></h2>
  <p>
    キャンペーンの参加には、ピーチの会員番号、NFCまたはQRコード対応のスマートフォンが必要となります。
  </p>
  <p>
    応募完了前に機種変更をした場合、それまで貯めたスタンプはすべて無効となります。
  </p>
  <p>
    キャンペーン期間中にスタンプを貯めていたWebブラウザのcookieを削除した場合、それまで貯めたスタンプはすべて無効となりますのでご注意ください。
  </p>
  <p>
    特別なアプリのインストールは不要ですが、フィーチャーフォンでは参加いただけません。スマートフォン（iPhone・Androidなど）をお使いください。
  </p>
  <p>
    OS標準のWebブラウザ（iPhone：Safari、Android：Chrome推奨）でcookie機能を有効にして参加ください。OS標準のWebブラウザ以外では正しく動作しないことがあります。
  </p>
  <p>
    プライベートブラウザONの状態では、スタンプは貯まりません。
  </p>
  <p>
    スタンプを貯めるには同じWebブラウザをお使いください。Webブラウザが変わるとスタンプが貯まりません。
  </p>
  <p>
    QRコードを読み取る際には、毎回、同じアプリをご使用ください。
  </p>
  <p>
    スタンプサイトへのアクセスや、応募にかかるパケット通信料はお客さまのご負担となります。
  </p>
  <p>
    セキュリティアプリをインストールされている場合、エラーが発生する恐れがございます。エラーが発生された際は、セキュリティアプリを無効化して再度お試しください。
  </p>
  <div class="close-btn01"><p><a href="#close-modal" rel="modal:close" class="btn01 green">とじる</a></p></div>
</div>

<?php // modal#modal-nfc?>
<div id="modal-nfc" class="modal">
  <h2 class="title01"><span>NFCについて</span></h2>
  <p>
    NFCはスマホをICチップにタッチするだけでサイトにアクセスする仕組みです。タッチラリーキャンペーンはこのNFCの仕組みを利用しています。
  </p>
  <p>
    Androidスマホの場合、ロックを解除しホーム画面でタッチしてください。iPhoneXS、XS MAX、XRの場合、ロック画面のままNFCが使えます。
  </p>
  <p>
    iPhone7,8,9,Xの場合、NFCリーダーアプリを利用してNFCが使えます。（別途インストールが必要です）
  </p>
  <p>
    反応が悪い場合は以下の方法をお試しください。
  </p>
  <p>
    反応に時間がかかる場合がありますので、少し長めにタッチしてみてください。
  </p>
  <p>
    Androidスマホの場合、NFCがオフになっている場合がありますのでご確認ください。
  </p>
  <p>
    NFCセンサーの位置にプレートが当たっていない場合がありますので位置を変えてみてください。（iPhoneの場合、センサーはスマホ上部にあります）
  </p>
  <p>
    上記で解決しない場合はQRコードをご利用ください。
  </p>
  <div class="close-btn01"><p><a href="#close-modal" rel="modal:close" class="btn01 green">とじる</a></p></div>
</div>

<?php // modal#modal-peach-point?>
<div id="modal-peach-point" class="modal">
  <h2 class="title01"><span>引き換え方法</span></h2>
  <p>
    引換所（空港の観光カウンター）にて、引き換え用スマプレを読み込こむ
  </p>
  <p>
    ▼
  </p>
  <p>
    ピーチポイントに引き換えのお申込みフォームが開く
  </p>
  <p>
    ▼
  </p>
  <p>
    お名前、会員情報を入力する
  </p>
  <p>
    ▼
  </p>
  <p>
    ピーチポイントが付与される
  </p>
  <div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13755836.723801471!2d129.42931353209104!3d32.676509158712236!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34674e0fd77f192f%3A0xf54275d47c665244!2z5pel5pys!5e0!3m2!1sja!2sjp!4v1562541400919!5m2!1sja!2sjp" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
  <div class="close-btn01"><p><a href="#close-modal" rel="modal:close" class="btn01 green">とじる</a></p></div>
</div>

<?php // modal#modal-already-touch?>
<div id="modal-already-touch" class="modal">
  <p class="text-large text-center">
    既にタッチ済みです。
  </p>
  <div class="close-btn02"><p><a href="#close-modal" rel="modal:close" class="btn01 green">とじる</a></p></div>
</div>

<?php // modal#modal-form?>
<div id="modal-form" class="modal">
  <form action="#" method="post">
    <p><label><input type="radio" name="member" value="会員">会員</label></p>
    <p><label><input type="radio" name="member" value="非会員">非会員</label></p>
    <p><input type="text" name="your-name" placeholder="お名前"></p>
    <p><input type="email" name="your-mail" placeholder="メールアドレス"></p>
    <div class="close-btn02">
      <p><input type="submit" value="次へ" class="btn01 green"></p>
      <p><a href="#close-modal" rel="modal:close" class="btn01 green">とじる</a></p>
    </div>
  </form>
</div>

<?php // modal#modal-form-confirm?>
<div id="modal-form-confirm" class="modal">
  <form action="#" method="post">
    <p class="text-large text-center">
      入力したお名前
    </p>
    <p class="text-large text-center">
      入力したメールアドレス
    </p>
    <div class="close-btn02">
      <p><input type="submit" value="送信" class="btn01 green"></p>
      <p><a href="#close-modal" rel="modal:close" class="btn01 green">とじる</a></p>
    </div>
  </form>
</div>

<?php // modal#modal-form-thanks?>
<div id="modal-form-thanks" class="modal">
  <p class="text-large text-center">
    ご参加ありがとう<br>
    ございました。
  </p>
  <figure class="text-center"><img src="img/top/img01.png" alt=""></figure>
  <p class="text-large text-center">
    引き続き、釧路ひがし<br>
    北海道チャレンジラリーを<br>
    お楽しみください。
  </p>
  <div class="close-btn02"><p><a href="#close-modal" rel="modal:close" class="btn01 green">とじる</a></p></div>
</div>

</body>

</html>