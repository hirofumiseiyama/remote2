<!DOCTYPE html>
<html>

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <!--og-->
  <meta property="og:title" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">
  <meta property="og:type" content="website">
  <meta property="og:description" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">
  <meta property="og:url" content="https://example.com/">
  <meta property="og:site_name" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">

  <meta charset="UTF-8">
  <title>チャレンジポイント｜スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー</title>
  <meta name="keywords" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">
  <meta name="description" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

  <link rel="canonical" href="https://example.com/">
  <link rel="stylesheet" href="css/main.css" />
  <link rel="apple-touch-icon" href="img/common/apple.png">

  <!--image max5-->
  <meta property="og:image" content="https://example.com/img/common/OG.png">

  <!--Twitter-->
  <meta name="twitter:card" content="summary_large_image">

  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css">
  <script src="//unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
  <script src="js/common.js"></script>
  <script src="js/filter.js"></script>

  <script>
  // masonryの設定
  jQuery.event.add(window, "load", function(){
    $('.shop-list').masonry({
      itemSelector: '.list-item',
      columnWidth: 150,
      isFitWidth: true,
      isAnimated: true
    });
  });

// sortタイトルの設定
  $(function () {
    $('.sort-nav li').click(function () {
      if ($(this).hasClass('all')) { // すべてを選択した場合
        var greet = document.getElementById('cat-name')
        greet.innerHTML = "チャレンジポイント一覧";
      } else { // フィルターを実行
        filter_cat = ($(this).data('json')).cat;
        var greet = document.getElementById('cat-name')
        greet.innerHTML = filter_cat + " 一覧";
      }
    });
  });
  </script>
</head>

<body class="challenge-point">
  <header id="global-head" class="small-header">
    <h1><a href="index.php"><img src="img/common/main-logo02.png" alt="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー"></a></h1>
    <ul class="sort-nav">
      <li class="all">All</li>
      <li data-json='{"cat":"グルメ"}'>グルメ</li>
      <li data-json='{"cat":"体験/見学"}'>体験/見学</li>
      <li data-json='{"cat":"ショッピング"}'>ショッピング</li>
      <li data-json='{"cat":"観光施設"}'>観光施設</li>
    </ul>
  </header>

  <main>
    <div class="map-btn"><a href="#"><span>参加店をMAPからさがす</span></a></div>
    <h2 class="title01"><span id="cat-name">チャレンジポイント一覧</span></h2>
    <ul class="shop-list">
      <li class="list-item" data-json='{"cat":"グルメ"}'><a href="#">
        <figure><img src="img/challenge_point/dummy.jpg" alt=""></figure>
        <h3>グルメ店舗名店舗名</h3>
        <p>テキストテキストテキストテキストテキストテキストテキストテキスト</p>
      </a></li>
      <li class="list-item" data-json='{"cat":"グルメ"}'><a href="#">
        <figure><img src="img/challenge_point/dummy.jpg" alt=""></figure>
        <h3>グルメ店舗名店舗名</h3>
        <p>テキストテキストテキストテキストテキスト</p>
      </a></li>
      <li class="list-item" data-json='{"cat":"体験/見学"}'><a href="#">
        <figure><img src="img/challenge_point/dummy.jpg" alt=""></figure>
        <h3>体験/見学店舗名</h3>
        <p>テキストテキストテキストテキストテキスト</p>
      </a></li>
      <li class="list-item" data-json='{"cat":"ショッピング"}'><a href="#">
        <figure><img src="img/challenge_point/dummy.jpg" alt=""></figure>
        <h3>ショッピング店舗名</h3>
        <p>テキストテキストテキストテキストテキストテキストテキストテキスト</p>
      </a></li>
      <li class="list-item" data-json='{"cat":"観光施設"}'><a href="#">
        <figure><img src="img/challenge_point/dummy.jpg" alt=""></figure>
        <h3>観光施設店舗名</h3>
        <p>テキストテキストテキストテキストテキスト</p>
      </a></li>
      <li class="list-item" data-json='{"cat":"グルメ"}'><a href="#">
        <figure><img src="img/challenge_point/dummy.jpg" alt=""></figure>
        <h3>グルメ店舗名店舗名</h3>
        <p>テキストテキストテキストテキストテキストテキストテキストテキスト</p>
      </a></li>
      <li class="list-item" data-json='{"cat":"グルメ"}'><a href="#">
        <figure><img src="img/challenge_point/dummy.jpg" alt=""></figure>
        <h3>グルメ店舗名店舗名</h3>
        <p>テキストテキストテキストテキストテキストテキストテキストテキスト</p>
      </a></li>

      <li class="list-item" data-json='{"cat":"体験/見学"}'><a href="#">
        <figure><img src="img/challenge_point/dummy.jpg" alt=""></figure>
        <h3>体験/見学店舗名</h3>
        <p>テキストテキストテキストテキストテキストテキストテキストテキスト</p>
      </a></li>
      <li class="list-item" data-json='{"cat":"ショッピング"}'><a href="#">
        <figure><img src="img/challenge_point/dummy.jpg" alt=""></figure>
        <h3>ショッピング</h3>
        <p>テキストテキストテキストテキストテキスト</p>
      </a></li>

      <li class="list-item" data-json='{"cat":"観光施設"}'><a href="#">
        <figure><img src="img/challenge_point/dummy.jpg" alt=""></figure>
        <h3>観光施設店舗名</h3>
        <p>テキストテキストテキストテキストテキストテキストテキストテキスト</p>
      </a></li>
      <li class="list-item" data-json='{"cat":"体験/見学"}'><a href="#">
        <figure><img src="img/challenge_point/dummy.jpg" alt=""></figure>
        <h3>体験/見学店舗名</h3>
        <p>テキストテキストテキストテキストテキスト</p>
      </a></li>
      <li class="list-item" data-json='{"cat":"ショッピング"}'><a href="#">
        <figure><img src="img/challenge_point/dummy.jpg" alt=""></figure>
        <h3>ショッピング店舗名</h3>
        <p>テキストテキストテキストテキストテキストテキストテキストテキスト</p>
      </a></li>
      <li class="list-item" data-json='{"cat":"観光施設"}'><a href="#">
        <figure><img src="img/challenge_point/dummy.jpg" alt=""></figure>
        <h3>観光施設店舗名</h3>
        <p>テキストテキストテキストテキストテキスト</p>
      </a></li>
    </ul>
  </main>

  <footer id="global-foot">
    <nav>
      <ul class="container">
        <li><a href="index.php">TOP</a></li>
        <li><a href="challenge_point.php">チャレンジポイント</a></li>
      </ul>
    </nav>
    <div class="container">
      <p class="privacy"><a href="#">プライバシーポリシー</a></p>
      <small class="copylight">Copyright(C) 2019 Peach Aviation 株式会社</small>
    </div>
  </footer>

  <div id="page-top"><a href="#global-head"><img src="img/common/page-top.svg" alt="Page Top"></a></div>
</body>

</html>