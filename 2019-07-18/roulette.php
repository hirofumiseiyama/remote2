<!DOCTYPE html>
<html>

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <!--og-->
  <meta property="og:title" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">
  <meta property="og:type" content="website">
  <meta property="og:description" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">
  <meta property="og:url" content="https://example.com/">
  <meta property="og:site_name" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">

  <meta charset="UTF-8">
  <title>ルーレット｜スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー</title>
  <meta name="keywords" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">
  <meta name="description" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

  <link rel="canonical" href="https://example.com/">
  <link rel="stylesheet" href="css/main.css" />
  <link rel="apple-touch-icon" href="img/common/apple.png">

  <!--image max5-->
  <meta property="og:image" content="https://example.com/img/common/OG.png">

  <!--Twitter-->
  <meta name="twitter:card" content="summary_large_image">

  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css">
  <script src="js/common.js"></script>
</head>

<body class="roulette">
  <header id="global-head" class="small-header">
    <h1><a href="index.php"><img src="img/common/main-logo02.png" alt="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー"></a></h1>
  </header>

  <main>
    <div class="container">
      <figure class="text-center"><img src="img/roulette/roulette.png" alt=""></figure>
      <div class="btn"><a href=#" class="btn01">ルーレットをまわす</a></div>
    </div>

    <?php /* モーダル確認用 */?>
    <div class="text-center" style="margin-top: 150px;">
      <div class="container">
        <h2 class="title01"><span>モーダル表示確認</span></h2>
        <p><a href="#modal-hit01" class="noScroll btn01" rel="modal:open">1等当選</a></p>
        <p><a href="#modal-hit02" class="noScroll btn01" rel="modal:open">2等当選</a></p>
        <p><a href="#modal-hit03" class="noScroll btn01" rel="modal:open">3等当選</a></p>
        <p><a href="#modal-hit04" class="noScroll btn01" rel="modal:open">4等当選</a></p>
        <p><a href="#modal-miss01" class="noScroll btn01" rel="modal:open">はずれ</a></p>
      </div>
    </div>
  </main>

  <footer id="global-foot">
    <nav>
      <ul class="container">
        <li><a href="index.php">TOP</a></li>
        <li><a href="challenge_point.php">チャレンジポイント</a></li>
      </ul>
    </nav>
    <div class="container">
      <p class="privacy"><a href="#">プライバシーポリシー</a></p>
      <small class="copylight">Copyright(C) 2019 Peach Aviation 株式会社</small>
    </div>
  </footer>

  <div id="page-top"><a href="#global-head"><img src="img/common/page-top.svg" alt="Page Top"></a></div>


<?php // ----------
// モーダルウィンドウ
// ---------------- ?>
<?php // modal#modal-hit01?>
<div id="modal-hit01" class="modal hit">
  <h2 class="title">おめでとうございます!</h2>
  <figure><img src="img/common/point01.svg" alt="1等 10,000pts"></figure>
  <p>
    引換所でピーチポイントと<br>
    交換してください
  </p>
  <div class="close-btn02"><p><a href="#close-modal" rel="modal:close" class="btn01 green">とじる</a></p></div>
</div>

<?php // modal#modal-hit02?>
<div id="modal-hit02" class="modal hit">
  <h2 class="title">おめでとうございます!</h2>
  <figure><img src="img/common/point02.svg" alt="2等 3,000pts"></figure>
  <p>
    引換所でピーチポイントと<br>
    交換してください
  </p>
  <div class="close-btn02"><p><a href="#close-modal" rel="modal:close" class="btn01 green">とじる</a></p></div>
</div>

<?php // modal#modal-hit03?>
<div id="modal-hit03" class="modal hit">
  <h2 class="title">おめでとうございます!</h2>
  <figure><img src="img/common/point03.svg" alt="3等 1,000pts"></figure>
  <p>
    引換所でピーチポイントと<br>
    交換してください
  </p>
  <div class="close-btn02"><p><a href="#close-modal" rel="modal:close" class="btn01 green">とじる</a></p></div>
</div>

<?php // modal#modal-hit04?>
<div id="modal-hit04" class="modal hit">
  <h2 class="title">おめでとうございます!</h2>
  <figure><img src="img/common/point04.svg" alt="4等 200pts"></figure>
  <p>
    引換所でピーチポイントと<br>
    交換してください
  </p>
  <div class="close-btn02"><p><a href="#close-modal" rel="modal:close" class="btn01 green">とじる</a></p></div>
</div>

<?php // modal#modal-miss01?>
<div id="modal-miss01" class="modal miss">
  <h2 class="title">はずれました...</h2>
  <p>
    またの挑戦おまちしております。
  </p>
  <div class="close-btn02"><p><a href="#close-modal" rel="modal:close" class="btn01 black">とじる</a></p></div>
</div>

</body>

</html>