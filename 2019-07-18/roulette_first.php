<!DOCTYPE html>
<html>

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <!--og-->
  <meta property="og:title" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">
  <meta property="og:type" content="website">
  <meta property="og:description" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">
  <meta property="og:url" content="https://example.com/">
  <meta property="og:site_name" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">

  <meta charset="UTF-8">
  <title>アンケート｜スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー</title>
  <meta name="keywords" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">
  <meta name="description" content="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

  <link rel="canonical" href="https://example.com/">
  <link rel="stylesheet" href="css/main.css" />
  <link rel="apple-touch-icon" href="img/common/apple.png">

  <!--image max5-->
  <meta property="og:image" content="https://example.com/img/common/OG.png">

  <!--Twitter-->
  <meta name="twitter:card" content="summary_large_image">

  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css">
  <script src="js/common.js"></script>

  <script>
    $(function() {
      btn_disabled();

      $('input[name="questionnaire"]').change(function() {
        btn_disabled();
      });

      function btn_disabled() {
        var btn_id = $('#submit');
        var radio_val = $('input[name="questionnaire"]:checked').val();

        if(!radio_val) {
          btn_id.prop('disabled', true);
        } else {
          btn_id.prop('disabled', false);
        }
      }
    });
  </script>
</head>

<body class="roulette-first">
  <header id="global-head" class="small-header">
    <h1><a href="index.php"><img src="img/common/main-logo02.png" alt="スマホでタッチするだけ 釧路ひがし北海道チャレンジラリー"></a></h1>
  </header>

  <main>
    <div class="container">
      <form action="roulette.php" method="post" class="questionnaire">
        <h2 class="title01"><span>アンケート</span></h2>
        <p><input type="radio" id="answer01" name="questionnaire" value="旅行"><label for="answer01" class="btn01">旅行</label></p>
        <p><input type="radio" id="answer02" name="questionnaire" value="出張"><label for="answer02" class="btn01">出張</label></p>
        <p><input type="radio" id="answer03" name="questionnaire" value="地元"><label for="answer03" class="btn01">地元</label></p>
        <div class="btn"><input id="submit" type="submit" value="ルーレットチャレンジ" disabled="disabled"></div>
      </form>
    </div>
  </main>

  <footer id="global-foot">
    <nav>
      <ul class="container">
        <li><a href="index.php">TOP</a></li>
        <li><a href="challenge_point.php">チャレンジポイント</a></li>
      </ul>
    </nav>
    <div class="container">
      <p class="privacy"><a href="#">プライバシーポリシー</a></p>
      <small class="copylight">Copyright(C) 2019 Peach Aviation 株式会社</small>
    </div>
  </footer>

  <div id="page-top"><a href="#global-head"><img src="img/common/page-top.svg" alt="Page Top"></a></div>
</body>

</html>